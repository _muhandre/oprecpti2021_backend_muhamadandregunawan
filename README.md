# Blog Posts

* Blog Post object
```
{
  owner: <user_object>
  id: integer
  title: string
  content: string
  date_created: datetime(iso 8601)
}
```
**GET /posts**
----
  Returns all Blog Posts in the system.
* **URL Params**  
  None
* **Data Params**  
  None
* **Headers**  
  Content-Type: application/json  
* **Success Response:**  
* **Code:** 200  
  **Content:**  
```
{
  count: integer
  next: string
  previous: string
  results: [
           {<blogpost_object>},
           {<blogpost_object>},
           {<blogpost_object>}
         ]
}
```

**GET /posts/:id**
----
  Returns the specified Blog Post.
* **URL Params**  
  *Required:* `id=[integer]`
* **Data Params**  
  None
* **Headers**  
  Content-Type: application/json  
  Authorization: Bearer `<OAuth Token>`
* **Success Response:** 
* **Code:** 200  
  **Content:**  `{ <blogpost_object> }` 
* **Error Response:**  
  * **Code:** 404  
  **Content:** `{ detail : "Not found." }`  

**POST /posts**
----
  Creates a new Blog Post and returns the new object.
* **URL Params**  
  None
* **Headers**  
  Content-Type: application/json  
* **Data Params**  
```
  {
    title: string,
    content: string
  }
```
* **Success Response:**  
* **Code:** 201  
  **Content:**  `{ <blogpost_object> }` 
* **Error Response:**  
  * **Code:** 403  
  **Content:** `{ error : error : "You are unauthorized to make this request." }`

**PATCH /posts/:id**
----
  Updates fields on the specified Blog Post and returns the updated object.
* **URL Params**  
  *Required:* `id=[integer]`
* **Data Params**  
```
  {
  	title: string,
    content: string,
  }
```
* **Headers**  
  Content-Type: application/json  
  Authorization: Bearer `<OAuth Token>`
* **Success Response:** 
* **Code:** 200  
  **Content:**  `{ <blogpost_object> }`  
* **Error Response:**  
  * **Code:** 404  
  **Content:** `{ detail : "Not found" }`  
  OR   
  * **Code:** 403  
  **Content:** `{ error : error : "You are unauthorized to make this request." }`

**DELETE /posts/:id**
----
  Deletes the specified Blog Post.
* **URL Params**  
  *Required:* `id=[integer]`
* **Data Params**  
  None
* **Headers**  
  Content-Type: application/json  
  Authorization: Bearer `<OAuth Token>`
* **Success Response:** 
  * **Code:** 204 
* **Error Response:**  
  * **Code:** 404  
  **Content:** `{ detail : "Not found" }` 
  OR   
  * **Code:** 403  
  **Content:** `{ error : error : "You are unauthorized to make this request." }`