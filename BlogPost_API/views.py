from rest_framework import permissions
from .permissions import IsOwnerOrReadOnly
from rest_framework import viewsets
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter
from .models import BlogPost
from .serializers import BlogPostSerializer

class BlogPostViewSet(viewsets.ModelViewSet):
    """
    API endpoint so that users from this web can see what Blog Post is there, and can also CURD it
    """
    queryset = BlogPost.objects.all()
    serializer_class = BlogPostSerializer
    filter_backends = (DjangoFilterBackend, OrderingFilter)
    filter_fields = ('title', 'date_created')
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)
