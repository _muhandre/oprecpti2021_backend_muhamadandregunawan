from django.db import models

# Create your models here.
class BlogPost(models.Model):
    owner = models.ForeignKey('auth.User', related_name='blogposts', on_delete=models.CASCADE)
    highlighted = models.TextField()
    title = models.CharField(max_length = 200)
    content = models.CharField(max_length = 5000)
    date_created = models.DateField(auto_now_add = True)