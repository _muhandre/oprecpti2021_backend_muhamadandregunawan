from django.urls import include, path
from rest_framework import routers
from .views import BlogPostViewSet

app_name = 'BlogPost_API'

router = routers.DefaultRouter()
router.register(r'posts', BlogPostViewSet)

urlpatterns = [
    path('', include(router.urls)),
]