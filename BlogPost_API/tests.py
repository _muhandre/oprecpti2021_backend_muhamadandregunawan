from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APITestCase, APIClient
# from rest_framework.test import APIClient
from .models import BlogPost
from rest_framework.test import APIRequestFactory, force_authenticate
from django.contrib.auth.models import User

class BlogPostTests(APITestCase):
    def setUp(self):
        self.client = APIClient()
        user = User.objects.create_user('dummy1', 'dummy1@gmail.com', 'dummy1')
        user.save()
        self.client.login(username = 'dummy1', password = 'dummy1')

    def tearDown(self):
        self.client.logout()

    def test_post_list_url_is_exist(self):
        response = self.client.get('/posts/')
        self.assertEqual(response.status_code, 200)
        
    def test_create_post(self):
        request = self.client.post('/posts/', {'title': 'post title 1', 'content' : 'post content 1'}, format = 'json')
        self.assertEqual(request.status_code, 201)
        self.assertEqual(BlogPost.objects.count(), 1)
        html_request = request.content.decode('utf-8')
        self.assertIn("post title 1", html_request)

    def test_single_post_with_same_user(self):
        request = self.client.post('/posts/', {'title': 'post title 2', 'content' : 'post content 2'}, format = 'json')
        response = self.client.get('/posts/1/')
        self.assertEqual(response.status_code, 200)

    def test_single_post_with_different_user(self):
        request = self.client.post('/posts/', {'title': 'post title 2', 'content' : 'post content 2'}, format = 'json')
        self.client.logout()
        user2 = User.objects.create_user('dummy2', 'dummy2@gmail.com', 'dummy2')
        user2.save()
        self.client.login(username = 'dummy2', password = 'dummy2')
        response = self.client.get('/posts/1/')
        self.assertEqual(response.status_code, 200)