from rest_framework import serializers
from .models import BlogPost

class BlogPostSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        model = BlogPost
        fields = ['id', 'owner', 'title', 'content', 'date_created']
