from django.apps import AppConfig


class BlogpostApiConfig(AppConfig):
    name = 'BlogPost_API'
