from rest_framework import permissions


class IsOwnerOrReadOnly(permissions.BasePermission):
    """
    Permission to edit a post for the user who owns it
    """

    def has_object_permission(self, request, view, obj):
        """ 
        Menentukan method mana saja yang boleh dilakukan oleh user
        """
        if request.method in permissions.SAFE_METHODS:
            return True

        return obj.owner == request.user